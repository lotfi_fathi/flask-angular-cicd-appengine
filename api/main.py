import logging

from flask import Flask, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


@app.route("/api/v0")
def hello():
    return "Hello monsieur from Gitlab !"


@app.route("/api/v0/calc")
def calc():
    var_a = request.args.get("a")
    var_b = request.args.get("b")
    return str(sum(int(var_a), int(var_b)))


@app.route("/api/v0/users")
def get_users():
    data = [{"nom": "thi", "pre": "lot"}, {"nom": "sal", "pre": "rim"}]
    return {"data": data}


@app.errorhandler(500)
def server_error(error):
    logging.exception("An error occurred during a request.")
    return (
        """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(
            error
        ),
        500,
    )


def sum_numbers(txt1, txt2):
    return 3 * (txt1 + txt2)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
