import unittest

from main import sum_numbers


class Test(unittest.TestCase):
    def test_sum(self):
        value = sum_numbers(4, 5)
        self.assertEqual(value, 27)


if __name__ == "__main__":
    unittest.main()
