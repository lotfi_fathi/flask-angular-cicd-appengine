import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseUrl:string = 'https://api-dot-fluid-analogy-364409.ew.r.appspot.com/api/v0/';

  constructor(private httpClient: HttpClient) { }

  listUsers() {
    return this.httpClient.get<any>(this.baseUrl + 'users');
  }
}
