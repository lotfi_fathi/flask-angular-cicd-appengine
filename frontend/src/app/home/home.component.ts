import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

interface User {
  nom: string;
  prenom: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  users: User[] = [];

  constructor(private apiService: ApiService){}

  ngOnInit(): void {
    this.apiService.listUsers().subscribe(
      res => {
        console.log(res.data);
        this.users = res.data;
      }
    )
  }

}
